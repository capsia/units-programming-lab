def sum_csv(file_name):
    total = None;
    handle = open(file_name, "r")
    handle.readline()
    for line in handle:
        parsed = line.split(",")
        try:
            val = float(parsed[1])
            if (total is None):
                total = 0
            total += val
        except:
            pass
    return total

#sum_csv("shampoo_sales.csv")
