# An exeption needs to be declared for autograding.live
class ExamException(Exception):
    pass

class InvalidArgumentTypesException(ExamException):
    pass

class NotLogicallyCoherentException(ExamException):
    pass

# The class calculating the average of intervals with known length
class MovingAverage():
    # Get interval length called window
    def __init__(self, window: int):
        if not isinstance(window, int) or not window > 0:
            raise InvalidArgumentTypesException(
                    "__init__ in MovingAverage accepts self: MovingAverage, window: unsigned int > 0")
        self.window = window
    # Calculate average value of the intervals
    def compute(self, data):
        if not isinstance(data, list) or not len([num for num in data if not isinstance(num, float) and not isinstance(num, int) and not isinstance(num, complex)]) == 0:
                raise InvalidArgumentTypesException(
                        "compute in MovingAverage accepts self: MovingAverage, data: [int|float|complex]")
        if len(data) < self.window:
            raise NotLogicallyCoherentException("Not enough data for selected window")
        return [sum(data[i:i+self.window])/self.window for i, num in enumerate(data) if (i <= len(data) - self.window)]

# exec: should be commented before running on autograding.live
#a = MovingAverage(4)
#print(a.compute([1,2,3,4,5,6,7,8.6+4.6J,2.2,10,11+4J,12,13]))
