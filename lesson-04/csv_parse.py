class CSVFile():
    def __init__(self, name):
        self.name = name
    def get_data(self):
        handle = open(self.name, "r")
        handle.readline()
        out = []
        for line in handle:
            splitLine = []
            for field in line.split(","):
                splitLine.append(field.strip())
            out.append(splitLine)
        return out

#testObject = CSVFile("shampoo_sales.csv")
#print(testObject.get_data())
