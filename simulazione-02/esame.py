# An exeption needs to be declared for autograding.live
class ExamException(Exception):
    pass

class InvalidArgumentTypesException(ExamException):
    pass

class NotLogicallyCoherentException(ExamException):
    pass

# The class calculating the average of intervals with known length
class Diff():
    # Get ratio to divide by
    def __init__(self, ratio = 1):
        if not (isinstance(ratio, int) or isinstance(ratio, float) or isinstance(ratio, complex)):
            raise InvalidArgumentTypesException(
                    "__init__ in Diff accepts self: Diff, ratio: int|float|complex")
        if ratio <= 0:
            raise NotLogicallyCoherentException("Cannot divide by zero")
        self.ratio = ratio
    # Calculate difference between adjacent numbers
    def compute(self, data):
        if not isinstance(data, list) or not len([num for num in data if not isinstance(num, float) and not isinstance(num, int) and not isinstance(num, complex)]) == 0:
                raise InvalidArgumentTypesException(
                        "compute in Diff accepts self: Diff, data: [int|float|complex]")
        if len(data) < 2:
            raise NotLogicallyCoherentException("Not enough data to compute a difference")
        return [(data[i+1]-data[i])/self.ratio for i, num in enumerate(data) if (i <= len(data) - 2)]

# exec: should be commented before running on autograding.live
#a = Diff()
#print(a.compute([1,2,3,4,5,6,7,8.6+4.6J,2.2,10,11+4J,12,13]))
