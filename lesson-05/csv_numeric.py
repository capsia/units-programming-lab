class CSVFile():
    def __init__(self, name):
        self.name = name
    def get_data(self):
        try:
            handle = open(self.name, "r")
            handle.readline()
            out = []
            for line in handle:
                splitLine = []
                for field in line.split(","):
                    splitLine.append(field.strip())
                out.append(splitLine)
            return out
        except FileNotFoundError as e:
            print("Whoops... Errore! Mona!")

class NumericalCSVFile(CSVFile):
    def get_data(self):
        data = super().get_data()
        for i, line in enumerate(data):
            for j, field in enumerate(line):
                if j == 0: continue
                try:
                    data[i][j] = float(field)
                except TypeError as e:
                    print("Whoops... Errore! Mona!")
                except ValueError as e:
                    print("Whoops... Errore! Mona!")
        return data

#testObject = CSVFile("shampoo_sales.csv")
#print(testObject.get_data())
