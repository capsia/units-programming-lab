# An exeption needs to be declared for autograding.live
class ExamException(Exception):
    pass

# Helper function to check parsability of a string as a type
def isparsable(verification_type, value):
    try:
      verification_type(value)
      return True
    except:
      return False

# Helper function to verify that two timestamps are contained in the same day
def same_day(timestamp_one, timestamp_two):
    return timestamp_one // 86400 == timestamp_two // 86400

# Helper function to check a time series
def check_time_series(time_series):
    # Verify data types and list lengths
    if not isinstance(time_series, list):
        raise ExamException(
            "Malformed time series, time series is expected to be a list")
    if not all([
        isinstance(e, list) # elements of the list are lists
        and len(e) >= 2 # each list contains 2 elements
        and isinstance(e[0], int) # the first is of type int
        and (isinstance(e[1], int) or isinstance(e[1], float)) # the second is int or float
        for e in time_series
    ]):
        raise ExamException(
            "Malformed time series, time series is expected contain lists of [int, float]")
    # Verify strictly sorted list ASC
    if not all([line[0] < time_series[i + 1][0] for i, line in enumerate(time_series[:-1])]):
        raise ExamException(
            "Data is not sorted correctly or duplicated for the same timestamp")

# A simple csv reader and parser
class CSVFile():
    def __init__(self, name):
        if not isinstance(name, str) or len(name) == 0:
            raise ExamException(
                    "__init__ in CSVFile accepts self: CSVFile, name: str")
        self.name = name

    def get_data(self):
        try:
            handle = open(self.name, "r")
            handle.readline()
            return [[field.strip() for field in line.split(",")] for line in handle]
        except:
            raise ExamException("Cannot parse the given file")

# Time series parser based on CSVFile
class CSVTimeSeriesFile(CSVFile):
    def get_data(self):
        data = super().get_data()
        # Parse ints and floats if parsable or fail silently
        parsed = [
            [int(e[0]), float(e[1]), *e[2:]]
            for e in data
            if len(e) >= 2 and isparsable(int, e[0]) and isparsable(float, e[1])
        ]
        # Verify time series (raises an exception if the time series is malformed)
        check_time_series(parsed);
        return parsed

# The function to compute the temperature difference
def compute_daily_max_difference(time_series):
    # Verify time series (raises an exception if the time series is malformed)
    check_time_series(time_series);
    # Filter the first line of each day
    day_initial_values = [
        line
        for i, line in enumerate(time_series)
        if i == 0 or not same_day(line[0], time_series[i - 1][0])
    ]
    # Create a list of values for each day from the initial values
    days = [
        [line[1] for line in time_series if same_day(day[0], line[0])]
        for day in day_initial_values
    ]
    # Calculate differences for each day
    return [max(day) - min(day) if len(day) > 1 else None for day in days]

# exec: should be commented before running on autograding.live
#a = CSVTimeSeriesFile("data.csv")
#print(compute_daily_max_difference(a.get_data()))
