class Model():
    def fit(self, data):
        raise NotImplementedError("Not implemented")
    def predict(self, data):
        raise NotImplementedError("Not implemented")

class IncrementModel(Model):
    def predict(self, data):
        if not isinstance(data, list):
            raise TypeError("Cannot predict for given data.")
        if len(data) < 2:
            raise Exception("Not enough data.")
        return data[-1]+(data[-1]-data[0])/(len(data)-1)

#targetFile = NumericalCSVFile("shampoo_sales.csv")
#data = targetFile.get_data()
#testObj = IncrementModel()
#print(testObj.predict([var[-1] for var in data]))
